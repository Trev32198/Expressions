#include <stdio.h>
#include "expressions.h"

int main(int argc, char *argv[])
{
	if (argc != 2)
	{
		// Number of arguments is incorrect.
		fprintf(stderr, "Expected one argument.\n");
		return 1;
	}
	else
	{
		// Expression evaluation code.
		printf("Evaluating: %s\n", argv[1]);
		unsigned char returnCode;
		long double result = eval(argv[1], &returnCode);
		if (returnCode == 1)
		{
			fprintf(stderr, "Evaluation failed (syntax may be invalid).\n");
			return 1;
		}
		else if (returnCode == 2)
		{
			fprintf(stderr, "Evaluation failed (could not allocate enough memory).\n");
			return 2;
		}
		else
		{
			printf("--> %Lf\n", result);
			return 0;
		}
	}

}
