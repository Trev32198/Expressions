#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/*
Expressions - A simple library for evaluating math expressions in strings
Copyright (C) 2017  Trevor Dumas

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

static char associativity(char *operatorStr)
{
	if (strcmp("^", operatorStr) == 0)
	{
		return 'r';
	}
	else
	{
		return 'l';
	}
}

static char precedence(char *operatorStr)
{
	if (strcmp("^", operatorStr) == 0)
	{
		return 3;
	}
	// '_' internally represents unary minus, which has the same precedence as * and /
	else if (strcmp("*", operatorStr) == 0 || strcmp("/", operatorStr) == 0 || strcmp("_", operatorStr) == 0)
	{
		return 2;
	}
	else if (strcmp("+", operatorStr) == 0 || strcmp("-", operatorStr) == 0)
	{
		return 1;
	}
	// Operator is a function token
	else
	{
		return 4;
	}
}

long double eval(char *expr, unsigned char *returnCode)
{
	// Count number of tokens in expression
	int nTokens = 0;
	for (int i = 0; i < strlen(expr); i++)
	{
		// Character at current position in expr is an operator, parenthesis, or comma
		if (expr[i] == '+' || expr[i] == '-' || expr[i] == '*' || expr[i] == '/' || expr[i] == '^' || expr[i] == '(' || expr[i] == ')' || expr[i] == ',')
		{
			nTokens++;
		}
		// Character at current position in expr is part of a number
		else if (expr[i] == '.' || (expr[i] >= '0' && expr[i] <= '9'))
		{
			nTokens++;
			// Advance looping index to end of number
			while (expr[i + 1] == '.' || (expr[i + 1] >= '0' && expr[i + 1] <= '9')) 
			{
				i++;
			}
		}
		// Character at current position in expr is part of a function name
		else if (expr[i] >= 'a' && expr[i] <= 'z')
		{
			nTokens++;
			// Advance looping index to end of function name
			while (expr[i + 1] >= 'a' && expr[i + 1] <= 'z')
			{
				i++;
			}
		}
	}
	// Create a new array of character strings to store tokens
	char *tokens[nTokens];
	for (int i = 0; i < nTokens; i++)
	{
		tokens[i] = NULL;
	}
	int tokenIndex = 0;
	// Store tokens in array
	for (int i = 0; i < strlen(expr); i++)
	{
		// Character at current position in expr is an operator, parenthesis, or comma
		if (expr[i] == '(' || expr[i] == ')' || expr[i] == '+' || expr[i] == '-' || expr[i] == '*' || expr[i] == '/' || expr[i] == '^' || expr[i] == ',')
		{
			// Make room for the character at the current position and the null character
			tokens[tokenIndex] = (char *)malloc(2 * sizeof(char));
			// Check if malloc failed
			if (tokens[tokenIndex] == NULL)
			{
				// Free memory, set *returnCode to 2, return 2
				for (int x = 0; x < nTokens; x++)
				{
					free(tokens[x]);
				}
				*returnCode = 2;
				return 2;
			}
			// If the operator is not '-', add the operator to tokens normally
			if (expr[i] != '-')
			{
				tokens[tokenIndex][0] = expr[i];
			}
			// If the operator is a unary '-', append '_' to tokens
			else if (tokenIndex == 0 || (tokenIndex >= 1 && (strcmp(tokens[tokenIndex - 1], "(") == 0 || strcmp(tokens[tokenIndex - 1], ",") == 0)))
			{
				tokens[tokenIndex][0] = '_';
			}
			// If the operator is a binary '-', append '-' to tokens
			else
			{
				tokens[tokenIndex][0] = '-';
			}
			// Add a null terminator to the operator string and increment tokenIndex
			tokens[tokenIndex][1] = '\0';
			tokenIndex++;
		}
		// Character at current position in expr is part of a number
		else if (expr[i] == '.' || (expr[i] >= '0' && expr[i] <= '9'))
		{
			// Record the starting index of the number
			int startIndex = i;
			// Advance looping index to end of number
			while (expr[i + 1] == '.' || (expr[i + 1] >= '0' && expr[i + 1] <= '9')) 
			{
				i++;
			}
			// Make room for the characters representing the number and the null character
			tokens[tokenIndex] = (char *)malloc(((i - startIndex) + 2) * sizeof(char));
			// Check if malloc failed
			if (tokens[tokenIndex] == NULL)
			{
				// Free memory, set *returnCode to 2, return 2
				for (int x = 0; x < nTokens; x++)
				{
					free(tokens[x]);
				}
				*returnCode = 2;
				return 2;
			}
			// Write number and null character to the allocated memory
			int k;
			for (k = startIndex; k <= i; k++)
			{
				tokens[tokenIndex][k - startIndex] = expr[k];
			}
			tokens[tokenIndex][k] = '\0';
			tokenIndex++;
		}
		// Character at current position in expr is part of a function name or constant
		else if (expr[i] >= 'a' && expr[i] <= 'z')
		{
			// Record the starting index of the function name
			int startIndex = i;
			// Advance looping index to end of function name
			while (expr[i + 1] >= 'a' && expr[i + 1] <= 'z')
			{
				i++;
			}
			// Make room for the characters representing the function name and the null character
			tokens[tokenIndex] = (char *)malloc(((i - startIndex) + 2) * sizeof(char));
			// Check if malloc failed
			if (tokens[tokenIndex] == NULL)
			{
				// Free memory, set *returnCode to 2, return 2
				for (int x = 0; x < nTokens; x++)
				{
					free(tokens[x]);
				}
				*returnCode = 2;
				return 2;
			}
			// Write function name or constant and null character to the allocated memory
			int k;
			for (k = startIndex; k <= i; k++)
			{
				tokens[tokenIndex][k - startIndex] = expr[k];
			}
			tokens[tokenIndex][k] = '\0';
			tokenIndex++;
		}
	}
	// Check to see if the syntax of the expression is invalid
	// (check to see if there are consecutive operators)
	for (int i = 1; i < nTokens; i++)
	{
		if ((strcmp(tokens[i], "+") == 0 || strcmp(tokens[i], "-") == 0 || strcmp(tokens[i], "_") == 0 || strcmp(tokens[i], "*") == 0 || strcmp(tokens[i], "/") == 0 || strcmp(tokens[i], "^") == 0) && (strcmp(tokens[i - 1], "+") == 0 || strcmp(tokens[i - 1], "-") == 0 || strcmp(tokens[i - 1], "_") == 0 || strcmp(tokens[i - 1], "*") == 0 || strcmp(tokens[i - 1], "/") == 0 || strcmp(tokens[i - 1], "^") == 0))
		{
			// Free memory, set *returnCode to 1, return 1
			for (int x = 0; x < nTokens; x++)
			{
				free(tokens[x]);
			}
			*returnCode = 1;
			return 1;
		}
	}
	// Use shunting yard algorithm to convert expression to postfix
	// See http://hackipedia.org/Algorithms/RPN/html/Shunting-yard%5falgorithm.htm
	char *outputStack[nTokens];
	char *operatorStack[nTokens];
	// Keep a record of where the last item is on each stack
	int lastOutput = -1;
	int lastOperator = -1;
	for (int i = 0; i < nTokens; i++)
	{
		// If the current token is a number, push it onto the output stack
		if ((tokens[i][0] >= '0' && tokens[i][0] <= '9') || (tokens[i][0] == '.'))
		{
			outputStack[++lastOutput] = tokens[i];
		}
		// If the current token is a constant, push it onto the output stack
		else if (strcmp("pi", tokens[i]) == 0 || strcmp("e", tokens[i]) == 0)
		{
			outputStack[++lastOutput] = tokens[i];
		}
		// If the current token is a function name, push it onto the operator stack
		else if (strcmp("sin", tokens[i]) == 0 || strcmp("cos", tokens[i]) == 0 || strcmp("tan", tokens[i]) == 0 || strcmp("sinh", tokens[i]) == 0 || strcmp("cosh", tokens[i]) == 0 || strcmp("tanh", tokens[i]) == 0 || strcmp("asin", tokens[i]) == 0 || strcmp("acos", tokens[i]) == 0 || strcmp("atan", tokens[i]) == 0 || strcmp("asinh", tokens[i]) == 0 || strcmp("acosh", tokens[i]) == 0 || strcmp("atanh", tokens[i]) == 0 || strcmp("abs", tokens[i]) == 0 || strcmp("mod", tokens[i]) == 0 || strcmp("min", tokens[i]) == 0 || strcmp("max", tokens[i]) == 0 || strcmp("ln", tokens[i]) == 0 || strcmp("log", tokens[i]) == 0 || strcmp("sqrt", tokens[i]) == 0 || strcmp("cbrt", tokens[i]) == 0 || strcmp("ceil", tokens[i]) == 0 || strcmp("floor", tokens[i]) == 0 || strcmp("round", tokens[i]) == 0 || strcmp("deg", tokens[i]) == 0 || strcmp("rad", tokens[i]) == 0)
		{
			operatorStack[++lastOperator] = tokens[i];
		}
		// If the current token is a comma / function arg separator, move operators from the operator stack to the output stack until a left parenthesis is at the top of the operator stack
		// If no left parenthesis is found, there was an error in entering the expression
		else if (strcmp(",", tokens[i]) == 0)
		{
			while (lastOperator >= 0 && strcmp("(", operatorStack[lastOperator]) != 0)
			{
				outputStack[++lastOutput] = operatorStack[lastOperator--];
			}
			if (lastOperator < 0)
			{
				// Free memory, set *returnCode to 1, return 1
				for (int x = 0; x < nTokens; x++)
				{
					free(tokens[x]);
				}
				*returnCode = 1;
				return 1;
			}
		}
		// If the current token is an operator
		else if (strcmp("+", tokens[i]) == 0 || strcmp("-", tokens[i]) == 0 || strcmp("_", tokens[i]) == 0 || strcmp("*", tokens[i]) == 0 || strcmp("/", tokens[i]) == 0 || strcmp("^", tokens[i]) == 0)
		{
			/* While:
			       (there is an operator or function at the top of operatorStack)
			       and ((the current operator token is left associative and its
			       precedence is less than or equal to that of the last operator on operatorStack)
			       or (the current operator token is right associative and its precedence is less than
		               the last operator on operatorStack))
			
			       {Pop last element off of operatorStack and push it onto outputStack} */
			while (lastOperator >= 0 && strcmp(operatorStack[lastOperator], "(") != 0 && ((associativity(tokens[i]) == 'l' && precedence(tokens[i]) <= precedence(operatorStack[lastOperator])) || (associativity(tokens[i]) == 'r' && precedence(tokens[i]) < precedence(operatorStack[lastOperator]))))
			{
				outputStack[++lastOutput] = operatorStack[lastOperator--];
			}
			// Push the current operator token to operatorStack
			operatorStack[++lastOperator] = tokens[i];
		}
		// If the current token is '(', append it to operatorStack
		else if (strcmp("(", tokens[i]) == 0)
		{
			operatorStack[++lastOperator] = tokens[i];
		}
		// If the current token is ')'
		else if (strcmp(")", tokens[i]) == 0)
		{
			// While the operator on top of operatorStack is not '(',
			// pop the last element off of operatorStack onto the outputStack
			// If the operatorStack is completely emptied and '(' is not found, there was an error in entering the expression
			while (lastOperator >= 0 && strcmp("(", operatorStack[lastOperator]) != 0)
			{
				outputStack[++lastOutput] = operatorStack[lastOperator--];
			}
			if (lastOperator < 0)
			{
				// Free memory, set *returnCode to 1, return 1
				for (int x = 0; x < nTokens; x++)
				{
					free(tokens[x]);
				}
				*returnCode = 1;
				return 1;
			}
			// Pop '(' from operatorStack, but don't add it to outputStack
			lastOperator--;
		}
	}
	// After all tokens have been processed,
	// pop all remaining elements from operatorStack onto outputStack
	// If '(' is encountered, there was an error in entering the expression
	while (lastOperator >= 0)
	{
		if (strcmp("(", operatorStack[lastOperator]) == 0)
		{
			for (int i = 0; i < nTokens; i++)
			{
				free(tokens[i]);
			}
			*returnCode = 1;
			return 1;
		}
		outputStack[++lastOutput] = operatorStack[lastOperator--];
	}
	// Evaluate postfix expression contained by outputStack
	// See https://en.wikipedia.org/wiki/Reverse_Polish_notation
	// Create a an array of char pointers to be used in processing the postfix expression
	long double numberStack[nTokens];
	int lastNumber = -1;
	// Loop once for each token in outputStack
	for (int i = 0; i <= lastOutput; i++)
	{
		// If the current token is a number, append it to numberStack
		if ((outputStack[i][0] == '.') || (outputStack[i][0] >= '0' && outputStack[i][0] <= '9'))
		{
			// If an error occurs in converting the string to a long double,
			// free memory, set *returnCode to 1 and return 1
		        if (sscanf(outputStack[i], "%Lf", &(numberStack[++lastNumber])) != 1)
			{
				for (int x = 0; x < nTokens; x++)
				{
					free(tokens[x]);
				}
				*returnCode = 1;
				return 1;
			}
		}
		// If the current token is a constant, append it to numberStack
		else if (strcmp("pi", outputStack[i]) == 0)
		{
			numberStack[++lastNumber] = 3.14159265358979323846;
		}
		else if (strcmp("e", outputStack[i]) == 0)
		{
			numberStack[++lastNumber] = 2.71828182845904523536;
		}
		// If the current token is not a number, it must be a function name or an operator
		// In this is the case, pop the number of arguments required by the function or operator from outputStack
		// Use these popped values and the function name or operator to make a computation
		// The result of this computation must then be appended to numberStack
		else
		{
			// Process operators and functions
			long double a, b, n;
			// Process unary operators
			// Unary minus
			if (strcmp("_", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = -numberStack[lastNumber--];
			}
			// Process binary operators
			// Addition
			else if (strcmp("+", outputStack[i]) == 0 && lastNumber >= 1)
			{
				// Doing this on one line (without variables a and b) makes the compiler
				// warn of potentially undefined behavior
				// So, for safety reasons, get the values to be added separately
				a = numberStack[lastNumber--];
				b = numberStack[lastNumber--];
				n = a + b;
			}
			// Subtraction
			else if (strcmp("-", outputStack[i]) == 0 && lastNumber >= 1)
			{
				a = numberStack[lastNumber--];
				b = numberStack[lastNumber--];
				n = b - a;
			}
			// Multiplication
			else if (strcmp("*", outputStack[i]) == 0 && lastNumber >= 1)
			{
				a = numberStack[lastNumber--];
				b = numberStack[lastNumber--];
				n = a * b;
			}
			// Division
			else if (strcmp("/", outputStack[i]) == 0 && lastNumber >= 1)
			{
				a = numberStack[lastNumber--];
				b = numberStack[lastNumber--];
				n = b / a;
			}
			// Exponents
			else if (strcmp("^", outputStack[i]) == 0 && lastNumber >= 1)
			{
				a = numberStack[lastNumber--];
				b = numberStack[lastNumber--];
				n = pow(b, a);
			}
			// Process functions that need a single argument
			// Degrees function (radians to degress)
			else if (strcmp("deg", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = (180 / 3.14159265358979323846) * numberStack[lastNumber--];
			}
			// Radians function (degrees to radians)
			else if (strcmp("rad", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = (3.14159265358979323846 / 180) * numberStack[lastNumber--];
			}
			// Sine
			else if (strcmp("sin", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = sin(numberStack[lastNumber--]);
			}
			// Cosine
			else if (strcmp("cos", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = cos(numberStack[lastNumber--]);
			}
			// Tangent
			else if (strcmp("tan", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = tan(numberStack[lastNumber--]);
			}
			// Hyperbolic sine
			else if (strcmp("sinh", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = sinh(numberStack[lastNumber--]);
			}
			// Hyperbolic cosine
			else if (strcmp("cosh", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = cosh(numberStack[lastNumber--]);
			}
			// Hyperbolic tangent
			else if (strcmp("tanh", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = tanh(numberStack[lastNumber--]);
			}
			// Sine inverse
			else if (strcmp("asin", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = asin(numberStack[lastNumber--]);
			}
			// Cosine inverse
			else if (strcmp("acos", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = acos(numberStack[lastNumber--]);
			}
			// Tangent inverse
			else if (strcmp("atan", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = atan(numberStack[lastNumber--]);
			}
			// Hyperbolic sine inverse
			else if (strcmp("asinh", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = asinh(numberStack[lastNumber--]);
			}
			// Hyperbolic cosine inverse
			else if (strcmp("acosh", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = acosh(numberStack[lastNumber--]);
			}
			// Hyperbolic tangent inverse
			else if (strcmp("atanh", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = atanh(numberStack[lastNumber--]);
			}
			// Absolute value
			else if (strcmp("abs", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = abs(numberStack[lastNumber--]);
			}
			// Natural logarithm
			else if (strcmp("ln", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = log(numberStack[lastNumber--]);
			}
			// Square root
			else if (strcmp("sqrt", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = sqrt(numberStack[lastNumber--]);
			}
			// Cube root
			else if (strcmp("cbrt", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = cbrt(numberStack[lastNumber--]);
			}
			// Ceiling function
			else if (strcmp("ceil", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = ceil(numberStack[lastNumber--]);
			}
			// Floor function
			else if (strcmp("floor", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = floor(numberStack[lastNumber--]);
			}
			// Round
			else if (strcmp("round", outputStack[i]) == 0 && lastNumber >= 0)
			{
				n = round(numberStack[lastNumber--]);
			}
			// Process functions with two arguments
			// Modular division function
			else if (strcmp("mod", outputStack[i]) == 0 && lastNumber >= 1)
			{
				a = numberStack[lastNumber--];
				b = numberStack[lastNumber--];
				n = fmod(b, a);
			}
			// Min function
			else if (strcmp("min", outputStack[i]) == 0 && lastNumber >= 1)
			{
				a = numberStack[lastNumber--];
				b = numberStack[lastNumber--];
				n = fmin(a, b);
			}
			// Max function
			else if (strcmp("max", outputStack[i]) == 0 && lastNumber >= 1)
			{
				a = numberStack[lastNumber--];
				b = numberStack[lastNumber--];
				n = fmax(a, b);
			}
			// Logarithm function
			else if (strcmp("log", outputStack[i]) == 0 && lastNumber >= 1)
			{
				a = numberStack[lastNumber--];
				b = numberStack[lastNumber--];
				n = log(a) / log(b);
			}
			// If the operator or function is invalid, free allocated memory,
			// set returnCode to 1, and return 1
			else
			{
				for (int x = 0; x < nTokens; x++)
				{
					free(tokens[x]);
				}
				*returnCode = 1;
				return 1;
			}
			numberStack[++lastNumber] = n;
		}
	}
	// Free memory
	for (int i = 0; i < nTokens; i++)
	{
		free(tokens[i]);
	}
	// If more than one number remains on the stack, there was something wrong with the expression
	// Set *returnCode to 1 and return 1
	if (lastNumber != 0)
	{
		*returnCode = 1;
		return 1;
	}
	*returnCode = 0;
	return numberStack[0];	
}
