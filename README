Expressions is a C library for evaluating math expressions that are stored in character strings.
To use the library in a program, simply include the expressions.h header in it and call the
provided eval function. However, be sure to compile the program with the "-lm" flag
(if using gcc) to link to the math libraries, which are needed for this library to work.

The eval function is declared in expressions.h as follows:
long double eval(char *expr, unsigned char *returnCode)

In the declaration above, expr is a pointer to a character string containing an expression to
evaluate and returnCode is a pointer to an unsigned char. When the function is called, it will
attempt to evaluate the expression in the character string pointed to by expr. Upon success,
the calculated value is returned and the unsigned char pointed to by returnCode is set to 0.
If evaluation of the expression fails because of any syntax related issue, the unsigned char
pointed to by returnCode will be set to 1 and eval will immediately return 1. If evaluation
fails because not enough memory can be allocated, the unsigned char pointed to by returnCode
is set to 2 and eval immediately returns 2.


Supported operators:
+
- (both negation and subtraction)
*
/
^
Note: operators cannot be consecutive in an expression.
For example: "5 + (-5)" is valid, but "5 + -5" is not.

Supported constants:
pi - 3.14159...
e - 2.71828...

Supported functions (trigonometric, arguments are expected to be in radians):
sin(x)
cos(x)
tan(x)
sinh(x)
cosh(x)
tanh(x)
asin(x)
acos(x)
atan(x)
asinh(x)
acosh(x)
atanh(x)

Supported functions (miscellaneous):
deg(x) - converts x radians to degrees
rad(x) - converts x degrees to radians
abs(x) - absolute value of x
ln(x) - natural logarithm of x
sqrt(x) - square root of x
cbrt(x) - cube root of x
ceil(x) - round x up to an integer
floor(x) - round x down to an integer
round(x) - round x to closest integer
mod(x, y) - remainder of x / y
min(x, y) - smaller of x, y
max(x, y) - larger of x, y
log(x, y) - solve x ^ n == y for n


test_expressions.c provides a short example of how the expressions library can be used.
To compile this example program, run "gcc -Wall -lm test_expressions.c"
To try it, pass a math expression as the first argument to the compiled program.
Note: if the expression contains spaces, put it in quotes before passing it to the test program
so that the entire expression is considered one argument.